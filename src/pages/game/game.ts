import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GamePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html',
})
export class GamePage {
  public gameNumber
  private games;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.gameNumber = 0;

    this.games = this.navParams.get('games') || []

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GamePage');
  }

  winner(color) {
    if (color === 'red') {

    } else if (color === 'blue') {

    }
    this.gameNumber++;
  }

  nextGame() {
    this.gameNumber++;
  }

}
