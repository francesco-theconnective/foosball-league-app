import { Component } from '@angular/core';
import { IonicPage, NavController, Platform, NavParams, ViewController, ModalController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { Observable } from 'rxjs/Rx';

/**
 * Generated class for the FriendsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage {
  public friends: any[];
  public invitations: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _user: UserProvider,
    public modalCtrl: ModalController
  ) {
    this.friends = [
      { avatar: 'Francesco', name: 'Ludovico' },
      { avatar: 'Francesco', name: 'Ludovico' }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FriendsPage');
  }

  addFriend() {
    const modal = this.modalCtrl.create('AddFriendModalPage');
    modal.present();
  }

}