import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import { UserProvider } from '../../providers/user/user';
import { AuthorizationProvider } from '../../providers/authorization/authorization';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public games: any[];
  public statistics: any[];
  public invitations: any[];

  constructor(
    private modal: ModalController,
    public navCtrl: NavController,
    public _user: UserProvider,
    public auth: AuthorizationProvider
  ) {
    this.games = [];
    this.invitations = [
      { from: { firstname: 'Francesco' } },
      { from: { firstname: 'Ludovico' } }
    ]
    this.games = [
      { status: 'pending', day: 26, month: 'Apr' },
      { status: 'closed', day: 12, month: 'Mar' }
    ];
  }

  ionViewDidLoad() {
    Observable.forkJoin([
      this._user.getAllGames(),
      this._user.getAllInvitation(),
      this._user.getAllStatistics()
    ]).subscribe(
      response => {
        this.games = response[0];
        this.invitations = response[1];
        this.statistics = response[2];
      })

  }

  doFbLogin() {
    this.auth.fbLogin().then(() => {
      if (this.auth.isAuthenticated())
        this.navCtrl.setRoot('DashboardPage');
    }).catch(console.error)
  }

  showModalAppInfo() {

  }
  showPrivacy() {

  }
  showModalTerms() {

  }
}
