import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthorizationProvider } from '../../providers/authorization/authorization';

/**
 * Generated class for the RoyalRumblePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-royal-rumble',
  templateUrl: 'royal-rumble.html',
})
export class RoyalRumblePage {
  public players: any[];
  public games: any[];
  public me: any;
  constructor(
    private navCtrl: NavController,
    private _authSvc: AuthorizationProvider,
    private navParams: NavParams) {
    this.players = this.navParams.get('selectedUsers') || [
      { firstname: 'Portiere N°3 (Pistillo)', offline: true, avatar: 'http://www.viewshub.com:8080/img/avatar/Avatar4.png' },
      { firstname: 'Portiere N° 2 (Simone)', offline: true, avatar: 'http://www.viewshub.com:8080/img/avatar/Avatar4.png' },
      { firstname: 'Ludovico', offline: true, avatar: 'http://www.viewshub.com:8080/img/avatar/Avatar4.png' }
    ];

    this.players.push(this._authSvc.getUser());

    this.games = [
      [this.players[0], this.players[1], this.players[2], this.players[3]],
      [this.players[0], this.players[2], this.players[1], this.players[3]],
      [this.players[0], this.players[3], this.players[1], this.players[2]]
    ]

    setTimeout(() => this.players[0].offline = false, 1000);
    setTimeout(() => this.players[1].offline = false, 2000);
    setTimeout(() => this.players[2].offline = false, 3000);
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad RoyalRumblePage', );
  }

  notAllAccepted() {
    let allAccepted = true;
    this.players.forEach(p => allAccepted = allAccepted && !p.offline);
    return !allAccepted;
  }

  play() {
    this.navCtrl.setRoot('GamePage', {
      games: this.games
    });
  }
}
