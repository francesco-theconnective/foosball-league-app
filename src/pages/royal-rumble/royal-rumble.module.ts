import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoyalRumblePage } from './royal-rumble';

@NgModule({
  declarations: [
    RoyalRumblePage,
  ],
  imports: [
    IonicPageModule.forChild(RoyalRumblePage),
  ],
})
export class RoyalRumblePageModule {}
