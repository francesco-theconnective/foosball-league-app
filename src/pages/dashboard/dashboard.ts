import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  public games: any[];
  public statistics: any[];
  public invitations: any[];

  constructor(
    public navCtrl: NavController,
    public _user: UserProvider
  ) {
    this.games = [];
    this.invitations = [
      { from: { firstname: 'Francesco' } },
      { from: { firstname: 'Ludovico' } }
    ]
    this.games = [
      { status: 'pending', day: 26, month: 'Apr' },
      { status: 'closed', day: 12, month: 'Mar' }
    ];
  }

  ionViewDidLoad() {
    Observable.forkJoin([
      this._user.getAllGames(),
      this._user.getAllInvitation(),
      this._user.getAllStatistics()
    ]).subscribe(
      response => {
        this.games = response[0];
        this.invitations = response[1];
        this.statistics = response[2];
      })
  }

  openGameModal() {
    this.navCtrl.push('GameModalPage');
  }

}
