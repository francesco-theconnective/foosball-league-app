import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GameModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game-modal',
  templateUrl: 'game-modal.html',
})
export class GameModalPage {
  public friends;

  constructor(private navCtrl: NavController, private navParams: NavParams) {
    this.friends = [
      { firstname: 'Portiere N° 2 (Simone)', offline: true, avatar: 'http://www.viewshub.com:8080/img/avatar/Avatar4.png', selected: false },
      { firstname: 'Pilla', offline: true, avatar: 'http://www.viewshub.com:8080/img/avatar/Avatar4.png', selected: false },
      { firstname: 'Portiere N°3 (Pistillo)', offline: true, avatar: 'http://www.viewshub.com:8080/img/avatar/Avatar4.png', selected: false },
      { firstname: 'Ludovico', offline: true, avatar: 'http://www.viewshub.com:8080/img/avatar/Avatar4.png', selected: false }
    ]
  }

  ionViewDidLoad() {
  }

  cannotInvite() {
    let counter = 0;
    this.friends.forEach(u => {
      if (u.selected) counter++;
    });

    return counter != 3;
  }

  userSelected() {
    this.navCtrl.setRoot('RoyalRumblePage', {
      selectedUsers: this.friends.filter(u => u.selected)
    });
  }


}
