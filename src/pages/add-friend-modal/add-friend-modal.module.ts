import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddFriendModalPage } from './add-friend-modal';

@NgModule({
  declarations: [
    AddFriendModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AddFriendModalPage),
  ],
})
export class AddFriendModalPageModule {}
