import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LeaderboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leaderboard',
  templateUrl: 'leaderboard.html',
})
export class LeaderboardPage {
  public players: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.players = [
      { avatar: 'Francesco', name: 'Ludovico' },
      { avatar: 'Francesco', name: 'Ludovico' }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaderboardPage');
  }

}
