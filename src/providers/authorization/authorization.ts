import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Configuration } from '../../app/app.constants';
import { Observable } from 'rxjs/Observable';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';

import 'rxjs/add/operator/map';

/*
  Generated class for the AuthorizationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthorizationProvider {
  private authData: any;
  private actionUrl: string;
  FB_APP_ID: number = 346437762416629;

  constructor(public http: Http,
    private storage: NativeStorage,
    private fb: Facebook,
    public config: Configuration
  ) {
    storage.getItem('authData').then(val => {
      this.authData = val || {
        user: null,
        token: ''
      };
    });
    this.actionUrl = config.apiBase + 'authorization/';

  }

  isAuthenticated() {
    return !!this.authData && !!this.authData.token
  }

  getToken() {
    // return this.authData.token
    return 'kj234iuk2jbrkjh';
  }

  getUser() {
    if (!this.authData) return { firstname: 'Francesco', offline: false, avatar: 'http://www.viewshub.com:8080/img/avatar/Avatar4.png' };
    return this.authData.user;
  }
  setUser(newUser) {
    this.authData.user = newUser;
    this.storage.setItem('authData', this.authData);
  }

  getUserid() {
    /*   if (!this.authData || !this.authData.user) return null;
      return this.authData.user.id; */
    return 1;
  }

  register(details) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();

      headers.append('Content-Type', 'application/json');
      this.http.post(this.actionUrl, details, { headers: headers })
        .subscribe(res => {

          let data = res.json();
          this.authData = data;
          this.storage.setItem('authData', data);
          resolve(data);
        }, (err) => {
          reject((err && err.json) ? err.json() : { msg: 'Errore' });
        });
    });
  }
  // eof - chreateAccount


  login(credentials) {

    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      this.http.post(this.actionUrl + 'login', credentials, { headers: headers })
        .subscribe(res => {
          let data = res.json();
          this.authData = data;
          this.storage.setItem('authData', data);
          // resolve(data);
          resolve(res.json());
        }, (err) => {
          reject((err && err.json) ? err.json() : { msg: 'Errore' });
        });

    });

  } // eof - login

  logout() {
    this.storage.setItem('authData', null);
    this.authData = null;
  } // eof - logout

  fbLogin() {
    return new Promise((resolve, reject) => {
      let permissions = new Array();
      // let nav = this.navCtrl;
      //the permissions your facebook app needs from the user
      permissions = ["public_profile", "email", "user_likes", "user_friends", "user_birthday", "user_location", "user_religion_politics"];

      this.fb.login(permissions)
        .then((response: FacebookLoginResponse) => {
          let headers = new Headers();
          headers.append('Content-Type', 'application/json');

          this.http.post(this.actionUrl + 'social/facebook', response.authResponse, { headers: headers })
            .subscribe(res => {
              this.authData = res.json();
              this.storage.setItem('authData', this.authData);
              resolve(this.authData);
            }, error => {
              reject(error);
            });
        }).catch(err => reject(err))
    });
  }

}
