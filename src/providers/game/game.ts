import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Configuration } from '../../app/app.constants';
import { Observable } from 'rxjs/Observable';
import { AuthorizationProvider } from '../authorization/authorization';

import 'rxjs/add/operator/map';

/*
  Generated class for the GameProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GameProvider {
  private actionUrl: string;

  constructor(public http: Http, private config: Configuration) {
    this.actionUrl = config.apiBase + 'games/';
  }

  public getAll<T>(): Observable<any> {
    return this.http.get(this.actionUrl);
  }
  public get<T>(id: number): Observable<any> {
    return this.http.get(this.actionUrl + id);
  }
  public create<T>(model: any): Observable<any> {
    return this.http.post(this.actionUrl, model);
  }

  public update<T>(id: number, model: any): Observable<any> {
    console.log(model)
    return this.http
      .put(this.actionUrl + id, model);
  }

  public delete<T>(id: number): Observable<any> {
    return this.http.delete(this.actionUrl + id);
  }

}
