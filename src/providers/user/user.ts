import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Configuration } from '../../app/app.constants';
import { Observable } from 'rxjs/Observable';
import { AuthorizationProvider } from '../authorization/authorization';

import 'rxjs/add/operator/map';



/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  private actionUrl: string;

  constructor(
    public http: Http,
    private config: Configuration,
    private auth: AuthorizationProvider
  ) {
    this.actionUrl = this.config.apiBase + 'user/';
  }

  updateAccount(id, details) {
    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', 'Bearer ' + this.auth.getToken());
      this.http.put(this.actionUrl + id, details, { headers: headers })
        .subscribe(res => {
          let data = res.json();
          this.auth.setUser(data);
          resolve(data);
        }, (err) => {
          reject((err && err.json) ? err.json() : { msg: 'Errore' });
        });
    });
  }

  getAllGames<T>(): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + this.auth.getToken());
    let hd = { headers: headers };
    return this.http.get(`${this.actionUrl}${this.auth.getUserid()}/game`, hd);
  }
  getAllInvitation<T>(): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + this.auth.getToken());
    let hd = { headers: headers };
    return this.http.get(`${this.actionUrl}${this.auth.getUserid()}/invitation`, hd);
  }
  getAllStatistics<T>(): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + this.auth.getToken());
    let hd = { headers: headers };
    return this.http.get(`${this.actionUrl}${this.auth.getUserid()}/stat`, hd);
  }
}
