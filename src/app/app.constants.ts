import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {

    public Server = 'http://localhost:6060/';
    public ApiUrl = 'api/v1/';
    public apiBase = this.Server + this.ApiUrl;
}
