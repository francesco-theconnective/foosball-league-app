import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LeaderboardPage } from '../pages/leaderboard/leaderboard';
import { MatchmakingPage } from '../pages/matchmaking/matchmaking';
import { ProfilePage } from '../pages/profile/profile';
import { PlayersPage } from '../pages/players/players';
import { FriendsPage } from '../pages/friends/friends';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Configuration } from './app.constants';
import { UserProvider } from '../providers/user/user';
import { GameProvider } from '../providers/game/game';
import { AuthorizationProvider } from '../providers/authorization/authorization';
import { HttpModule, Http } from '@angular/http';
import { InvitationProvider } from '../providers/invitation/invitation';
import { NativeStorage } from '@ionic-native/native-storage';
import { Facebook } from '@ionic-native/facebook';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FriendsPage,
    LeaderboardPage,
    MatchmakingPage,
    ProfilePage,
    PlayersPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FriendsPage,
    LeaderboardPage,
    MatchmakingPage,
    ProfilePage,
    PlayersPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Configuration,
    UserProvider,
    NativeStorage,
    Facebook,
    GameProvider,
    AuthorizationProvider,
    InvitationProvider
  ]
})
export class AppModule { }
